﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor (typeof(FieldOfView))]
public class FieldOfViewEditor : Editor {

    void OnSceneGUI()
    {
        FieldOfView fovPlayer = (FieldOfView)target;
        Handles.color = Color.green;
        Handles.DrawWireArc(fovPlayer.transform.position, Vector3.up, Vector3.forward, 360, fovPlayer.viewRadius);
        
        Vector3 viewAngleA = fovPlayer.DirFromAngle(-fovPlayer.viewAngle / 2f, false);
        Vector3 viewAngleB = fovPlayer.DirFromAngle(+fovPlayer.viewAngle / 2f, false);

        Handles.DrawLine(fovPlayer.transform.position, fovPlayer.transform.position + viewAngleA * fovPlayer.viewRadius);
        Handles.DrawLine(fovPlayer.transform.position, fovPlayer.transform.position + viewAngleB * fovPlayer.viewRadius);

        Handles.color = new Color(0, 1, 0, 0.2f);
        Handles.CubeCap(0, fovPlayer.transform.position, Quaternion.identity, 10);

        Handles.color = Color.red;
        foreach(Transform visibleTarget in fovPlayer.visibleTargets)
        {
            Handles.DrawLine(fovPlayer.transform.position, visibleTarget.position);
        }
    }
}
