﻿using UnityEngine;
using System.Collections;

public class Weapon
{

    int id;
    string name;
    string type;
    int ammo;
    float rateOfFire;

    public Weapon()
    {
        id = -1;
        name = "";
        type = "";
        ammo = -1;
        rateOfFire = -1;
    }

    public Weapon(int _id, string _name, string _type, int _ammo, float _rateOfFire)
    {
        id = _id;
        name = _name;
        type = _type;
        ammo = _ammo;
        rateOfFire = _rateOfFire;
    }

    public Weapon(Weapon tmp)
    {
        id = tmp.id;
        name = tmp.name;
        type = tmp.type;
        ammo = tmp.ammo;
        rateOfFire = tmp.rateOfFire;
    }
}
