﻿using UnityEngine;
using System.Collections;

public class WeaponManager : MonoBehaviour {

    public string name;
    public string type;
    public int id;
    public int ammo;
    public float rateOfFire;

    Weapon weapon;

    public Weapon Weapon
    {
        get
        {
            return weapon;
        }

        set
        {
            weapon = value;
        }
    }
    
    // Use this for initialization
    void Start () {
        weapon = new Weapon(id, name, type, ammo, rateOfFire);
	}

    public void DecreaseAmmo()
    {
        ammo--;
    }
}
