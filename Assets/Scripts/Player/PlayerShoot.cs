﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerShoot : MonoBehaviour {

    public GameObject bulletPrefab;
    public float bulletForce;
    public float cooldownBeforeShoot;

    public AudioClip shotSound;

    private float actualCooldown;
    private AudioSource audioSource;

    public Image reloadBar;

    PlayerWeaponControl weaponController;

	// Use this for initialization
	void Start () {
        weaponController = GetComponent<PlayerWeaponControl>();
        audioSource = GetComponent<AudioSource>();
        actualCooldown = 0;
        reloadBar.fillAmount = 1;
    }
	
	// Update is called once per frame
	void Update () {
        actualCooldown -= Time.deltaTime;

        ReloadWeaponBar();

        if (Time.timeScale < 1)
        {
            if (Time.timeScale == 0.01f)
                audioSource.pitch = 0.5f;
            else
                audioSource.pitch = Time.timeScale;
        }
        else
            audioSource.pitch = 1;

        if (Input.GetMouseButtonDown(0))
        {
            if (actualCooldown <= 0 && weaponController.CurrentHoldedWeapon != null )
            {
                if(weaponController.CurrentWeapon.ammo > 0)
                    ShootCurrentWeapon();
                else
                {
                    Debug.Log("No Ammo");
                }
            }
            
        }
        
	}

    void ShootCurrentWeapon()
    {
        audioSource.PlayOneShot(shotSound);
        GameObject bullet = (GameObject)Instantiate(bulletPrefab, weaponController.CurrentHoldedWeapon.transform.FindChild("GunSpawnPoint").transform.position, Quaternion.identity);
        bullet.transform.rotation = transform.rotation;
        bullet.GetComponent<Rigidbody>().AddForce(weaponController.CurrentHoldedWeapon.transform.transform.forward * bulletForce, ForceMode.Impulse);
        Destroy(bullet, 5);
        weaponController.CurrentWeapon.DecreaseAmmo();
        actualCooldown = cooldownBeforeShoot;
    }

    void ReloadWeaponBar()
    {
        if (actualCooldown > 0)
            reloadBar.fillAmount = 1f - (actualCooldown / cooldownBeforeShoot);
        else
            reloadBar.fillAmount = 1;
    }
}
