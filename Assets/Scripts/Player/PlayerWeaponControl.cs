﻿using UnityEngine;
using System.Collections;

public class PlayerWeaponControl : MonoBehaviour {

    public float throwForce;

    bool canPickUpWeapon;

    GameObject currentHoldedWeapon;
    GameObject currentPickableWeapon;

    WeaponManager currentWeapon;

    public GameObject CurrentHoldedWeapon
    {
        get
        {
            return currentHoldedWeapon;
        }

        set
        {
            currentHoldedWeapon = value;
        }
    }

    public WeaponManager CurrentWeapon
    {
        get
        {
            return currentWeapon;
        }

        set
        {
            currentWeapon = value;
        }
    }

    // Use this for initialization
    void Start () {
        foreach (Transform child in transform)
        {
            if (child.CompareTag("Weapon"))
                currentHoldedWeapon = child.gameObject;
        }
        //currentHoldedWeapon = transform.FindChild
        currentWeapon = CurrentHoldedWeapon.GetComponent<WeaponManager>();
	}
	
	// Update is called once per frame
	void Update () {
        currentPickableWeapon = FindClosestWeapon();

        if (Input.GetMouseButtonDown(1))
        {
            bool justThrown = CurrentHoldedWeapon == currentPickableWeapon;
            Debug.Log(justThrown);

            if(currentHoldedWeapon != null)
            {
                ThrowCurrentWeapon();
                CurrentHoldedWeapon = null;
                currentHoldedWeapon = null;
            }
            
            if(canPickUpWeapon && currentPickableWeapon != null && !justThrown)
            {
                PickUpWeapon();
            }
        }
    }

    void ThrowCurrentWeapon()
    {
        Transform currentWeapon = CurrentHoldedWeapon.transform;
        currentWeapon.GetComponent<Rigidbody>().isKinematic = false;
        currentWeapon.GetComponent<Rigidbody>().AddForce(currentWeapon.forward * throwForce, ForceMode.Impulse);
        currentWeapon.gameObject.layer = 14;
        currentWeapon.transform.parent = null;
        currentWeapon = null;
    }

    void PickUpWeapon()
    {
        Debug.Log("pick up");
        currentPickableWeapon.layer = 12;
        CurrentHoldedWeapon = currentPickableWeapon;
        currentPickableWeapon.GetComponent<Rigidbody>().isKinematic = true;
        currentPickableWeapon.transform.SetParent(transform);
        currentPickableWeapon.transform.localPosition = new Vector3(0, 0, 0.5f);
        currentPickableWeapon.transform.localRotation = Quaternion.identity;
        currentWeapon = currentPickableWeapon.GetComponent<WeaponManager>();
    }

    GameObject FindClosestWeapon()
    {
        float closestWeaponDistance = float.MaxValue;
        GameObject closestWeapon = null;
        GameObject[] weapons = GameObject.FindGameObjectsWithTag("Weapon");
        foreach (GameObject wp in weapons)
        {
            float tmpDistance = Vector3.Distance(wp.transform.position, transform.position);
            if(tmpDistance < closestWeaponDistance)
            {
                closestWeaponDistance = tmpDistance;
                closestWeapon = wp;
            }
        }

        if(closestWeaponDistance > 2)
        {
            canPickUpWeapon = false;
            return null;
        }
        else
        {
            canPickUpWeapon = true;
            return closestWeapon;
        }
    }
    
}
