﻿using UnityEngine;
using System.Collections;

public class PlayerControls : MonoBehaviour {

    public float moveSpeed = 5;
    public float maxSpeed = 10;

    public AnimationCurve timeScaleSampling;

    public GameObject cameraTarget;

    private Rigidbody rigidBody;
    private float moveHorizontal;
    private float moveVertical;
    private Vector3 velocity;


	// Use this for initialization
	void Start () {
        rigidBody = GetComponent<Rigidbody>();

	}
	
	// Update is called once per frame
	void Update () {
        moveHorizontal = Input.GetAxis("Horizontal");
        moveVertical = Input.GetAxis("Vertical");

        velocity = new Vector3(moveHorizontal, 0, moveVertical).normalized * moveSpeed;

        //Time.timeScale = timeScaleSampling.Evaluate((Mathf.Abs(moveHorizontal) + Mathf.Abs(moveVertical)) / 2f);

        if(moveHorizontal == 0 && moveVertical == 0)
        {
            rigidBody.velocity = new Vector3(0, 0, 0);
            Time.timeScale = 0.03f;
        }
        else if (moveHorizontal != 0 || moveVertical != 0)
        {
            if(Time.timeScale != 1)
            { 
                if (moveHorizontal != 0 && moveVertical == 0)
                {
                    Time.timeScale = timeScaleSampling.Evaluate((Mathf.Abs(moveHorizontal)));
                }
                else if (moveVertical != 0 && moveHorizontal == 0)
                {
                    Time.timeScale = timeScaleSampling.Evaluate((Mathf.Abs(moveVertical)));
                }
                else if(moveHorizontal != 0 || moveVertical != 0)
                {
                    Time.timeScale = timeScaleSampling.Evaluate((Mathf.Abs(moveHorizontal) + Mathf.Abs(moveVertical)) / 2f);
                }
            }
            if (Time.timeScale < 0.5f)
            {
                Time.timeScale = 0.5f;
            }
        }

        //Player looks at mouse position
        Vector3 v3T = Input.mousePosition;
        v3T.z = Mathf.Abs(Camera.main.transform.position.y - transform.position.y);
        v3T = Camera.main.ScreenToWorldPoint(v3T);
        transform.LookAt(v3T);

        /*if(moveHorizontal != 0 && moveVertical != 0)
            cameraTarget.transform.position = transform.position + new Vector3(moveHorizontal, 0, moveVertical) * 5;*/

    }
    
    void FixedUpdate()
    {
        rigidBody.MovePosition(rigidBody.position + velocity * Time.fixedDeltaTime);
    }
}
