﻿using UnityEngine;
using System.Collections;

public class EnemeyWeaponControl : MonoBehaviour {

    public float throwForce;

    bool canPickUpWeapon;

    GameObject currentHoldedWeapon;
    GameObject currentPickableWeapon;

    WeaponManager currentWeapon;

    public GameObject CurrentHoldedWeapon
    {
        get
        {
            return currentHoldedWeapon;
        }

        set
        {
            currentHoldedWeapon = value;
        }
    }

    public WeaponManager CurrentWeapon
    {
        get
        {
            return currentWeapon;
        }

        set
        {
            currentWeapon = value;
        }
    }

    // Use this for initialization
    void Start () {
        foreach (Transform child in transform)
        {
            if (child.CompareTag("Weapon"))
                currentHoldedWeapon = child.gameObject;
        }
        currentWeapon = CurrentHoldedWeapon.GetComponent<WeaponManager>();
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
