﻿using UnityEngine;
using System.Collections;

public class EnemyShoot : MonoBehaviour {

    public GameObject bulletPrefab;
    public float bulletForce;
    public float cooldownBeforeShoot;

    public AudioClip shotSound;

    private float actualCooldown;
    private AudioSource audioSource;

    EnemeyWeaponControl weaponController;

    // Use this for initialization
    void Start () {
        weaponController = GetComponent<EnemeyWeaponControl>();
        audioSource = GetComponent<AudioSource>();
        actualCooldown = 0.1f;
    }
	
	// Update is called once per frame
	void Update () {
        actualCooldown -= Time.deltaTime;

        if (Time.timeScale < 1)
        {
            if (Time.timeScale == 0.01f)
                audioSource.pitch = 0.5f;
            else
                audioSource.pitch = Time.timeScale;
        }
        else
            audioSource.pitch = 1;

        if (actualCooldown < 0 && GetComponent<EnemyMovement>().playerInSight && weaponController.CurrentHoldedWeapon != null)
        {
            audioSource.PlayOneShot(shotSound);
            GameObject bullet = (GameObject)Instantiate(bulletPrefab, weaponController.CurrentHoldedWeapon.transform.FindChild("GunSpawnPoint").transform.position, Quaternion.identity);
            bullet.transform.rotation = transform.rotation;
            bullet.GetComponent<Rigidbody>().AddForce(weaponController.CurrentHoldedWeapon.transform.forward * bulletForce, ForceMode.Impulse);
            Destroy(bullet, 5);
            actualCooldown = cooldownBeforeShoot;
        }

    }
}
