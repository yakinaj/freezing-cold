﻿using UnityEngine;
using System.Collections;

public class EnemyMovement : MonoBehaviour {

    public GameObject player;
    public float fieldOfViewAngle;
    public float range;

    public bool playerInSight;
    public LayerMask playerMask;

    // Use this for initialization
    void Start () {
        playerInSight = false;

	}
	
	// Update is called once per frame
	void Update () {

        Vector3 direction = player.transform.position - transform.position;
        float angle = Vector3.Angle(direction, transform.forward);

        if(angle < fieldOfViewAngle * 0.5f)
        {
            RaycastHit hit;
            if(Physics.Raycast(transform.position, direction.normalized, out hit, range, playerMask))
            {
                if(hit.collider.gameObject == player)
                {
                    playerInSight = true;
                    //Player looks at mouse position
                    transform.LookAt(player.transform.position);
                }
                else
                {
                    playerInSight = false;
                }
            }
        }
        
    }
}
