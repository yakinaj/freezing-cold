﻿using UnityEngine;
using System.Collections;

public class EnemyDeath : MonoBehaviour {

    EnemeyWeaponControl weaponController;

    // Use this for initialization
    void Start () {
        weaponController = GetComponent<EnemeyWeaponControl>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnCollisionEnter(Collision collider)
    {
        if (collider.gameObject.tag == "Bullet")
        { 
            if(weaponController.CurrentHoldedWeapon != null)
            {
                weaponController.CurrentHoldedWeapon.GetComponent<Rigidbody>().isKinematic = false;
                weaponController.CurrentHoldedWeapon.GetComponent<Rigidbody>().AddForce((transform.forward + new Vector3(Random.Range(-0.6f, 0.6f), 0, 0)) * weaponController.throwForce, ForceMode.Impulse);
                weaponController.CurrentHoldedWeapon.transform.parent = null;
            }
            Destroy(collider.gameObject);
            Destroy(this.gameObject);
        }
        if(collider.gameObject.tag == "Weapon")
        {
            if (weaponController.CurrentHoldedWeapon != null)
            {
                weaponController.CurrentHoldedWeapon.GetComponent<Rigidbody>().isKinematic = false;
                weaponController.CurrentHoldedWeapon.GetComponent<Rigidbody>().AddForce((transform.forward + new Vector3(Random.Range(-0.6f, 0.6f), 0, 0)) * weaponController.throwForce, ForceMode.Impulse);
                weaponController.CurrentHoldedWeapon.transform.parent = null;
                weaponController.CurrentHoldedWeapon = null;
                weaponController.CurrentWeapon = null;
            }
        }
    }
}
