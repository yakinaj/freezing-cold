﻿using UnityEngine;
using System.Collections;

public class SmoothFollowPlayer : MonoBehaviour {

    public GameObject target;
    public GameObject player;
    Vector3 mousePos;
    Vector3 targetPos;
    Camera cam;

    public float speed;

	// Use this for initialization
	void Start () {
        cam = Camera.main;
	}

    // Update is called once per frame
    void Update()
    {
        targetPos = new Vector3(target.transform.position.x, transform.position.y, target.transform.position.z);
        transform.position = targetPos;

        mousePos = cam.ScreenToWorldPoint(Input.mousePosition);
        mousePos.y = 0.5f;
        //float distancePlayerToMouse = Vector3.Distance(player.transform.position, mousePos);
        //mousePos = new Vector3(Mathf.Clamp(mousePos.x,-15,15))
        Vector3 dir = mousePos - player.transform.position;
        //dir.y = 0;
        transform.position = Vector3.Lerp(transform.position, transform.position + dir * 0.5f, 0.5f);
        
    }
}
